#  Starting point of an EnvVar driven config.rb

AppConfig[:db_url] = "jdbc:mysql://%s/%s?useUnicode=true&characterEncoding=UTF-8&user=%s&password=%s" % \
    [
    ENV['DB_SERVER'],
    ENV['DB_DB'],
    ENV['DB_USER'],
    ENV['DB_PASSWORD']
    ]


AppConfig[:enable_backend] = ENV['AS_COMPONENT'] == 'backend' || ENV['AS_COMPONENT'] == 'aio'
AppConfig[:enable_frontend] = ENV['AS_COMPONENT'] == 'frontend' || ENV['AS_COMPONENT'] == 'aio'
AppConfig[:enable_public] = ENV['AS_COMPONENT'] == 'public' || ENV['AS_COMPONENT'] == 'aio'
AppConfig[:enable_solr] = false
AppConfig[:enable_indexer] = ENV['AS_COMPONENT'] == 'indexer' || ENV['AS_COMPONENT'] == 'aio'
AppConfig[:enable_docs] = false
AppConfig[:enable_oai] = ENV['AS_COMPONENT'] == 'oai' || ENV['AS_COMPONENT'] == 'aio'

AppConfig[:backend_url] = ENV['BACKEND_URL']
AppConfig[:frontend_url] = ENV['FRONTEND_URL']
AppConfig[:public_url] = ENV['PUBLIC_URL']
AppConfig[:indexer_url] = ENV['INDEXER_URL']
AppConfig[:oai_url] = ENV['OAI_URL']
AppConfig[:solr_url] = ENV['SOLR_URL']

AppConfig[:search_user_secret] = ENV['SEARCH_USER_SECRET']
AppConfig[:staff_user_secret] = ENV['STAFF_USER_SECRET']
AppConfig[:public_user_secret] = ENV['PUBLIC_USER_SECRET']

AppConfig[:public_cookie_secret] = ENV['PUBLIC_COOKIE_SECRET']
AppConfig[:frontend_cookie_secret] = ENV['FRONTEND_COOKIE_SECRET']
if ENV['PUBLIC_PROXY_URL'] then
    AppConfig[:public_proxy_url] = ENV['PUBLIC_PROXY_URL']
end
if ENV['FRONTEND_PROXY_URL'] then
    AppConfig[:frontend_proxy_url] = ENV['FRONTEND_PROXY_URL']
end