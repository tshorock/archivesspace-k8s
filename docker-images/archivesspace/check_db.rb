require 'logger'

if $0 =~ /scripts[\/\\]rb[\/\\]check_db.rb$/
  # This script runs in two contexts: build/run as a part of development, and
  # setup-database.(sh|bat) from the distribution zip file.  Allow for both.
  require_relative '../../launcher/launcher_init'
end

require 'config/config-distribution'
require "db/db_migrator"

begin
  migration_logger = Logger.new($stderr)

  # Just log messages relating to the migration.  Otherwise we get a full SQL
  # trace...
  def migration_logger.error(*args)
    unless args.to_s =~ /SCHEMA_INFO.*does not exist/
      super
    end
  end

  def migration_logger.info(*args)
    if args[0].is_a?(String) && args[0] =~ /applying migration/
      super
    end
  end

  Sequel.connect(AppConfig[:db_url],
                 :max_connections => 1,
                 :loggers => [migration_logger]) do |db|

    puts "Checking migration status against #{AppConfig[:db_url_redacted]}"
    if DBMigrator.needs_updating?(db)
    # Note - this prints the value of the ArchivesSpace core migration status, but the #needs_updating? does take plugin migrations into account too
      puts "Not ready - currently at " +
           Sequel::Migrator::migrator_class(DBMigrator::MIGRATIONS_DIR)
               .new(db, DBMigrator::MIGRATIONS_DIR, {}).current.to_s
      exit false
    end
    puts "All done."

  end
rescue Sequel::AdapterNotFound => e

  if AppConfig[:db_url] =~ /mysql/
    libdir = File.expand_path(File.join(File.dirname($0), "..", "..", "lib"))

    puts <<EOF

You have configured ArchivesSpace to use MySQL but seem to be missing the MySQL
JDBC driver (#{e}).

Please download the latest version of 'mysql-connector-java-X.Y.Z.jar' and place
it in the following directory:

  #{libdir}

You can find the latest version at the following URL:

  http://mvnrepository.com/artifact/mysql/mysql-connector-java/

Once you have installed the MySQL connector, please re-run this script.

EOF
  else
    raise $!
  end
end
