# [ArchivesSpace](https://archivesspace.org).

A demonstration of ArchivesSpace as a Kubernetes-scaled application

## Installation

```
# helm repo add shorock https://shorock.github.io/helm-charts
# helm install shorock/archivesspace 
```

This will hold you in suspense for several minutes while doing initial database schema population (*setup-database.sh*), and the rest of the components (in a clustered mode) won't start up 
until the backend is running.  So, patience.

## Docker image

The docker-images/archivesspace defines a Docker image that pulls some basic 
ArchivesSpace config (DB vars) from the environment. The environment variable 
**AS_COMPONENT** needs to be 
one of **aio** (all in one), **frontend**, **backend**, **oai**, **public**, 
or **indexer**.  The one Docker image has all those roles.

The provided image does not contain the venerable Solr 4.x distributed with ArchivesSpace.  
The chart instead maps to separate Solr 8.x and MySQL containers.  
Right now I'm not depending on a Solr sub-chart.  
The current [best community Solr](https://hub.helm.sh/charts/incubator/solr) chart 
is still in incubator status, and depends 
on a Zookeeper sub-chart which is in conflict with newer Kubernetes versions. 
However, the chart in incubation would do replicas, sharding, etc. 
I'm doing none of those fancy things in this chart.  As it matures, this chart should
eventually migrate to a community Solr chart. 

## AIO vs Clustered mode

The default `values.yaml` will install one all-in-one Pod with all the ArchivesSpace code 
(along with 
MariaDB and Solr Pods). For a separated service model (and horizontal scale), 
install with `--set mode=clustered`. 

I set the all-in-one to be the default
for memory reasons. 
Clustered, each replica of each component has its own Java/Jetty stack, 
so 5 GiB total is really a minimum. The all-in-one still needs 2-3 GiB, but 
is not crazy for a decently provisioned *minikube*.   

## Customization and plugins

This chart contains a starting point [`config.rb`](https://gitlab.com/tshorock/archivesspace-k8s/tree/master/helm/archivesspace/as-config) 
that takes cues from environment variables.

To add AppConfig values:

1) You can use the configrb_additions Helm setting. This is a meant to be a multi-line string that appends to the default `config.rb`.
2) You can copy the config.rb 
and add your own AppConfig[] lines to it.  
Then add it as a custom ConfigMap and `--set customCM=yourConfigMap`.
3) You can add a custom config.rb to your customized Docker image (at /archivesspace/config/config.rb) 
and `--set useConfigMap=false`

Plugins (TODO): 3 ways could be pursued - 
1) An initContainer on every Pod that copies/git-clones the plugins to /archivesspace/plugins on launch.
2) Plugins in a ReadOnlyMany/ReadWriteMany persistent volume. 
3) Create and  custom Docker image with your plugins

## Clustered-mode scaling

You can horizontally scale up the Backend, Frontend and Public Deployments separately. 
Scaling the indexer is not directly implemented. If you want to use an 
[S3 store](https://archivesspace.github.io/archivesspace/user/configuring-archivesspace/#Index-state) 
or a tiny ReadWriteMany file store to share the indexer state, it could scale too.

## Caveats and out-of-scope warnings
Leaving the Ingresses up to you, dear implementer. Everything is just an HTTP NodePort by default.

Also not implemented: autoscaling, disruption budgets... none of it. 

Do pay attention to your NetworkPolicy issues in a shared k8s cluster - as 
implemented here Solr is its old entirely-unsecure self.  

## Configuration Options

The following table shows common configuration options for the ArchivesSpace helm chart:

| Parameter                  | Description                           | Default Value                                      |
| ---------------------------| ------------------------------------- | -------------------------------------------------- |
| `mode`                     | `allInOne` or `clustered`     | `allInOne`                                             |
| `useConfigMap` | true if you're mapping config.rb from a ConfigMap, either the default one or a custom one | `true` |
| `defaultAdminPassword` | set a password for the 'admin' user on initial startup. (AppConfig[:default_admin_password) |
| `configrb_additions` | a (multiline) string to add to the default ConfigMap for config.rb  | unset |
| `customCM` | the name of a pre-defined ConfigMap with a key config.rb | unset |
| `components.public` | Enable the public user interface module | `true` |
| `mariadb.enabled` | Should this install an in-cluster MariaDB | `true` |
| `mariadb.master.persistence.size` | Size of the PersistentVolumeClaim for an in-cluster MariaDB | `10Gi` |
| `mariadb.db.user.password` | Password for the `as` user if using a in-cluster MariaDB | `archivesspace` |
| `mariadb.rootUser.password` | Root password for in-cluster MariaDB | `archivesspace-root` |
| `externalMySQL.server` | Host (can include port) for an external MySQL-compatible database. Only used if `mariadb.enabled` is false above | |
| `externalMySQL.user` | User for an external MySQL-compatible database | |
| `externalMySQL.password` | Password for an external MySQL-compatible database | |
| `externalMySQL.database` | Database for an external MySQL-compatible database | |
| `solr.persistence.size` | Size of PVC for our Solr | `10Gi` | 
| `solr.persistence.storageClass` | Set a StorageClass for that PVC | default StorageClass | 

## TODOS

- handle plugins well (possibly an InitContainer)
- Solr -- *** [WARN] *** Your Max Processes Limit is currently 15274.
It should be set to 65000 to avoid operational disruption.
- solr healthcheck/liveness
- solr 8 schema issues/deprecation check
- shared indexer state file (minio for s3? - seems overkill) so indexer can scale up
